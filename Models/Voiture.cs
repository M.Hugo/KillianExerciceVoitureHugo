﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Models
{
    public class Voiture
    {
        /// <summary>
        /// Creation des champs "Id","Nom","Marque","Imatriculation" et "Proprietaire" de la classe "Voiture" avec leur propre constructeur.
        /// </summary>
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Marque { get; set; }
        public string Imatriculation { get; set; }
        public string Proprietaire { get; set; }
    }
}
