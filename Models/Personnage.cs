﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Models
{
    public class Personnage
    {
        /// <summary>
        /// Creation des champs "Id","Nom","Prenom","Age" de la classe "Personnage" avec leur propre constructeur.
        /// </summary>
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
    }
}
