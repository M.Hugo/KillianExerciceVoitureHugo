﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UsersVoitureHugo.Controllers
{
    using Models;
    using Services;

    /// <summary>
    /// Route "personnages" permet d'accéder aux fonctions liées à celui çi depuis l'url.
    /// exemple d'url en serveur local : "https://localhost:44374/personnages"
    /// Pour rappel, le nom des routes doit OBLIGATOIREMENT être mis aux pluriels et sans majuscule.
    /// </summary>
    [Route("personnages")]
    [ApiController]
    public class PersonnageController : ControllerBase
    {
        /// <summary>
        /// Création de l'attribut "service" pour l'injection de dépendance
        /// </summary>
        
        private PersonnageService service;

        /// <summary>
        /// Constructeur qui prend en params "PersonnageService service" pour l'injection de dépendance
        /// </summary>
        /// <param name="service"></param>
        public PersonnageController(PersonnageService service)
        {
            this.service = service;
        }

        /// <summary>
        /// La fonction "Sauvegarde" du controller retourne la fonction "Sauvegarde" du service approprié,
        /// et prend en params l'objet Personnage qu'il enregistre dans un tableau => [FromBody].
        /// </summary>
        /// <param name="p"></param>
        /// <returns>
        /// Si erreur il y a, le Try/Catch renvoi l'erreur 400 "BadRequest"
        /// </returns>
        [HttpPost]
        [Route("")]
        public IActionResult Sauvegarde([FromBody] Personnage p)
        {
            try
            {
                return Ok(this.service.Sauvegarde(p));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// La fonction "FindAll" retourne la fonction "TrouverTous" du service approprié et fais une 
        /// requête "HttpGet" au serveur pour retourner tous les personnages.
        /// </summary>
        /// <returns>
        /// Si erreur il y a, le Try/Catch renvoi l'erreur 400 "BadRequest"
        /// </returns>
        [HttpGet]
        [Route("")]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(this.service.TrouverTous());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// La fonction "FindById" retourne la fonction "TrouverUn" du service approprié et fais une 
        /// requête "HttpGet" au serveur pour retourner un personnage via son Id.
        /// </summary>
        /// <returns>
        /// Si l'utilisateur entre un ID qui n'existe pas, il renvoi l'erreur 404 NotFound, pour toute autre erreur c'est l'erreur 400 BadRequest"
        /// </returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok (this.service.TrouverUn(id));
            }
            catch (IndexOutOfRangeException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {   
                return this.ValidationProblem(e.Message);
            }
        }

        /// <summary>
        /// La fonction "Update" retourne la fonction "Modifier" du service approprié et fais une 
        /// requête "HttpPut" au serveur pour modifier un personnage.
        /// </summary>
        /// <returns>
        /// Si erreur il y a, le Try/Catch renvoi l'erreur 404 "NotFound"
        /// </returns>
        [HttpPut]
        [Route ("")]
        public IActionResult Update([FromBody] Personnage p)
        {
            try
            {
                return Ok(this.service.Modifier(p));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// La fonction "Delete" utilise la fonction "Supprimer" du service approprié et fais une 
        /// requête "HttpDelete" au serveur pour supprimer un personnage via son Id.
        /// </summary>
        /// <returns>
        /// Si erreur il y a, le Try/Catch renvoi l'erreur 404 "NotFound"
        /// </returns>
        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                this.service.Supprimer(id);
                return Ok();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}