﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Services
{
    using Models;
    public interface VoitureService
    {

        /// <summary>
        /// Creation de l'interface "VoitureService" et ses fonctions.
        /// </summary>
        public IEnumerable<Voiture> TrouverTous();
        public Voiture TrouverUn(int id);
        public Voiture Modifier(Voiture pv);
        public Voiture Sauvegarde(Voiture v);
        public void Supprimer(int id);
    }
}
