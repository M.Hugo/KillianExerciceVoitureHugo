﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Services.Implements
{
    using Models;
    using Repositories;

    /// <summary>
    /// La classe "VoitureImplementsService" hérite de l'interface "VoitureService"
    /// </summary>
    public class VoitureImplementsService : VoitureService
    {
        /// <summary>
        /// Création de l'attribut "repository" pour l'injection de dépendance
        /// </summary>
        private VoitureRepository repository;

        /// <summary>
        /// Constructeur qui prend en params "VoitureRepository repository" pour l'injection de dépendance
        /// </summary>
        /// <param name="repository"></param>
        public VoitureImplementsService(VoitureRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// La fonction "Modifier" du service retourne la fonction "Update" du repository approprié,
        /// et prend en params l'objet "Voiture" nommé "v".
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Voiture Modifier(Voiture v)
        {
            return this.repository.Update(v);
        }

        /// <summary>
        /// La fonction "Sauvegarde" du service retourne la fonction "Save" du repository approprié,
        /// et prend en params l'objet "Voiture" qu'il enregistre dans un tableau.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Voiture Sauvegarde(Voiture v)
        {
            return this.repository.Save(v);
        }

        /// <summary>
        /// La fonction "Supprimer" du service utilise la fonction "Delete" du repository approprié selon son id,
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }

        /// <summary>
        /// La fonction "TrouverTous" du service retourne la fonction "FindAll" du repository approprié
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Voiture> TrouverTous()
        {
            return this.repository.FindAll();
        }

        /// <summary>
        /// La fonction "TrouverUn" du service retourne la fonction "FindById" du repository approprié
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Voiture TrouverUn(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
