﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Services.Implements
{
    using Models;
    using Repositories;

    /// <summary>
    /// La classe "PersonnageImplementsService" hérite de l'interface "PersonnageService"
    /// </summary>
    public class PersonnageImplementsService : PersonnageService
    {
        /// <summary>
        /// Création de l'attribut "repository" pour l'injection de dépendance
        /// </summary>
        private PersonnageRepository repository;

        /// <summary>
        /// Constructeur qui prend en params "PersonnageRepository repository" pour l'injection de dépendance
        /// </summary>
        /// <param name="repository"></param>
        public PersonnageImplementsService(PersonnageRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// La fonction "Modifier" du service retourne la fonction "Update" du repository approprié,
        /// et prend en params l'objet "Personnage" nommé "p".
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Personnage Modifier(Personnage p)
        {
            return this.repository.Update(p);
        }

        /// <summary>
        /// La fonction "Sauvegarde" du service retourne la fonction "Save" du repository approprié,
        /// et prend en params l'objet "Personnage" nommé "p" qu'il enregistre dans un tableau.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Personnage Sauvegarde(Personnage p)
        {
            return this.repository.Save(p);
        }

        /// <summary>
        /// La fonction "Supprimer" du service utilise la fonction "Delete" du repository approprié selon son id,
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }

        /// <summary>
        /// La fonction "TrouverTous" du service retourne la fonction "FindAll" du repository approprié
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Personnage> TrouverTous()
        {
            return this.repository.FindAll();
        }

        /// <summary>
        /// La fonction "TrouverUn" du service retourne la fonction "FindById" du repository approprié
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Personnage TrouverUn(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
