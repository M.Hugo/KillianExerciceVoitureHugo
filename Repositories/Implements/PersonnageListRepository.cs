﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Repositories.Implements
{
    using Models;

    /// <summary>
    /// La classe "PersonnageListRepository" hérite de l'interface "PersonnageRepository"
    /// </summary>
    public class PersonnageListRepository : PersonnageRepository
    {
        /// <summary>
        /// instanciation d'une liste de personnage.
        /// </summary>
        private List<Personnage> personnages = new List<Personnage>();

        /// <summary>
        /// La fonction "Delete" prend en params un Id, et le supprime si l'id est null.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            this.personnages[id] = null;
        }

        /// <summary>
        /// La fonction "Delete" prend en params un objet Personnage, et le supprime si son id est null.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Personnage p)
        {
            this.personnages[p.Id] = null;
        }

        /// <summary>
        /// La fonction "FindAll retourne tous les personnages"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Personnage> FindAll()
        {
            return this.personnages.Where(p => p != null);
        }

        /// <summary>
        /// La fonction "FindById" prend en params un id et retourne un personnage via celui ci.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Personnage FindById(int id)
        {
            return this.personnages[id];
        }

        /// <summary>
        /// La fonction "Save" prend en params un objet "Personnage" nommé "p", il effectue une vérification de son Id dans la liste
        /// et si il n'y a pas de conflit, il l'ajoute et retourne l'objet.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Personnage Save(Personnage p)
        {
            p.Id = this.personnages.Count();
            this.personnages.Add(p);
            return p;
        }

        /// <summary>
        /// La fonction "Update" prend en params un objet "Personnage" nommé "p".
        /// Il effectue une vérification, si l'Id et le même que celui de l'objet il l'update et retourne l'objet.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Personnage Update(Personnage p)
        {
            this.personnages[p.Id] = p;
            return p;
        }
    }
}
