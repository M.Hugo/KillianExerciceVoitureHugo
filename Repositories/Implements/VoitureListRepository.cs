﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersVoitureHugo.Repositories.Implements
{
    using Models;

    /// <summary>
    /// La classe "VoitureListRepository" hérite de l'interface "VoitureRepository"
    /// </summary>
    public class VoitureListRepository : VoitureRepository
    {
        /// <summary>
        /// instanciation d'une liste de voiture.
        /// </summary>
        private List<Voiture> voitures = new List<Voiture>();

        /// <summary>
        /// La fonction "Delete" prend en params un Id, et le supprime si l'id est null.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            this.voitures[id] = null;
        }

        /// <summary>
        /// La fonction "Delete" prend en params un objet "Voiture", et le supprime si son id est null.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Voiture v)
        {
            this.voitures[v.Id] = null;
        }

        /// <summary>
        /// La fonction "FindAll retourne toutes les voitures"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Voiture> FindAll()
        {
            return this.voitures.Where(v => v != null);
        }

        /// <summary>
        /// La fonction "FindById" prend en params un id et retourne une voiture via celui ci.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Voiture FindById(int id)
        {
            return this.voitures[id];
        }

        /// <summary>
        /// La fonction "Save" prend en params un objet "Voiture" nommé "v", il effectue une vérification de son Id dans la liste
        /// et si il n'y a pas de conflit, il l'ajoute et retourne l'objet.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Voiture Save(Voiture v)
        {
            v.Id = this.voitures.Count();
            this.voitures.Add(v);
            return v;
        }

        /// <summary>
        /// La fonction "Update" prend en params un objet "Voiture" nommé "v".
        /// Il effectue une vérification, si l'Id et le même que celui de l'objet il l'update et retourne l'objet.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Voiture Update(Voiture v)
        {
            this.voitures[v.Id] = v;
            return v;
        }
    }
}
